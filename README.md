# ItemCompare

## Credits

Plugin réalisé pour la plate-forme EMAN (ENS-CNRS-Sorbonne nouvelle) par Vincent Buard (Numerizen) avec le soutien avec le soutien de l’IRIHS – Université de Rouen Normandie : https://irihs.univ-rouen.fr. Voir les explications sur le site [EMAN](https://eman-archives.org/EMAN/itemcompare)
